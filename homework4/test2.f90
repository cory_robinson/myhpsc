! $MYHPSC/homework4/test2.f90
!
! Copied from $MYHPSC/homework4/test1.f90.
!	The difference from test1.f90 is here I have added in the 
!	function f2 (an oscilatory function). I have also increased
!	the bounds of the do loop fo nvals here because the function
!	f2 converges slower than f.
!
! This program uses the quadrature module to approximate the
!	integral of the function defined as f2.  There is still the
!	option to approximate the function defined as f by just
!	changing the function argument in the call to error_table.
!
! test1.f90 Provided by RJ Leveque on 5/2/2013
! Modified to test2.f90 by Cory R Robinson on 5/7/2013
!
program test2

    use quadrature, only: trapezoid, error_table

    implicit none
    real(kind=8) :: a,b,int_true,int_true2
    integer :: nvals(12), i
    real(kind=8), parameter :: k = 1000

    a = 0.d0
    b = 2.d0
    int_true = (b-a) + (b**4 - a**4) / 4.d0
    int_true2 = (b-a) + (b**4 - a**4) / 4.d0 - (1.d0/k)*(COS(k*b) - COS(k*a))

    print 10, int_true2
 10 format("true integral: ", es22.14)
    print *, " "  ! blank line

    ! values of n to test:
    do i=1,12
        nvals(i) = 5 * 2**(i-1)
        enddo

    call error_table(f2, a, b, nvals, int_true2)

contains

    real(kind=8) function f(x)
        implicit none
        real(kind=8), intent(in) :: x 
        
        f = 1.d0 + x**3
    end function f
    
    
   	real(kind=8) function f2(x)
   		implicit none
   		real(kind=8), intent(in) :: x
   		real(kind=8), parameter :: k = 1000
   		
   		f2 = 1.d0 + x**3 + SIN(k*x)
   	end function f2
   		
   		

end program test2
