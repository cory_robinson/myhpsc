! $MYHPSC/homework4/quadrature.f90
!
! This module is used to approximate the integral of a function
!	using the trapezoid rule, and also outputs a table of
!	approximations showing convergence towards the true integral
!	value, along with absolute error values and ratio of errors.
!
! Created by Cory R Robinson on 5/3/2013
!

module quadrature

contains



real(kind=8) function trapezoid(f,a,b,n)
	implicit none
	real(kind=8), intent(in) :: a,b
	integer, intent(in) :: n
	real(kind=8) :: int_trapezoid, h, s, x_i
	integer :: i
	real(kind=8), external :: f
	
	h = (b-a) / (n)	! step values for defining the bounds of each trapezoid
	s = 0.d0	! initialize the sum for the do loop
	do i = 1, (n-1)
		x_i = a + h*i
		s = s + f(x_i)	! sum
		enddo
	int_trapezoid = h*(s + 0.5d0*(f(a) + f(b)))	! integral formula for trapezoid rule
	
end function trapezoid





subroutine error_table(f,a,b,nvals,int_true)
	real(kind=8), external :: f
	real(kind=8), intent(in) :: a,b,int_true
	integer, dimension(:), intent(in) :: nvals
	real(kind=8) :: int_trap, error, ratio, last_error
	integer :: n,i
	
	! This outputs a table with the values of n, the approximation of 
	!	the integral from the trapezoid function, the absolute error
	!	and the ratio of errors.
	print *, "      n	trapezoid	    error	 ratio"
	last_error = 0.d0
	do i=1,size(nvals)
		n = nvals(i)
		int_trap = trapezoid(f,a,b,n)
		error = ABS(int_trap - int_true)
		ratio = last_error / error
		last_error = error
		print 10, n, int_trap, error, ratio
10		format(i8, es22.14, es13.3, es13.3)	
		enddo
	
end subroutine error_table



end module quadrature
