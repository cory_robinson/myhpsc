
"""
$MYHPSC/homework2/hw2b.py

hw2b

Module for interpolation of a polynomial.
    This module contains functions for interpolation and plotting of polynomials
    given data points input by the user in a numpy array.  Users input arrays 
    xi and yi, and the output of the *_interp(xi,yi) functions is an array c, 
    which is the coefficient array of the interpolated polynomial. 
    
    *_interp(xi,yi):
        Arrays xi and yi must be of equal length.
        If xi and yi have length 3, use quad_interp(xi,yi)
        If xi and yi have length 4, use cubic_interp(xi,yi)
        If xi and yi have any length n, use poly_interp(xi,yi)
        Array xi must have unique values, otherwise *_interp(xi,yi) will return 
            "No Solutions" due to a singular matrix.
    
    plot_*(xi,yi):
        Opens a new window with the interpolated polynomial plotted 
        along with data points.
        
    test_*():
        Three tests are run on each *_interp(xi,yi)        
    
Modified by: Cory R Robinson on 04/15/2013

========================================================
EXAMPLE:

$ import hw2b
$ from numpy import array
$ xi = array([-1.,  0.,  2.])
$ yi = array([ 1., -1.,  7.])
$ c = hw2b.quad_interp(xi,yi)
...:   array([-1.,  0.,  2.])

========================================================


"""


import numpy as np
import matplotlib.pyplot as plt
from numpy.linalg import solve
from numpy.linalg import det



#########################################################################
#####     *_interp(xi,yi)     ###########################################

def quad_interp(xi,yi):
    """
    Quadratic interpolation.  Compute the coefficients of the polynomial
    interpolating the points (xi[i],yi[i]) for i = 0,1,2.
    Returns c, an array containing the coefficients of
      p(x) = c[0] + c[1]*x + c[2]*x**2.
    """

    # check inputs and print error message if not valid:

    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message

    error_message = "xi and yi should have length 3"
    assert len(xi)==3 and len(yi)==3, error_message
    

    # Set up linear system to interpolate through data points:
    A = np.vstack([np.ones(3), xi, xi**2]).T
    b = yi
    
    # Check for singular matrix and print error message if matrix is singular:
    matrix_error = "Input xi ==> Singular matrix. No solutions!"
    assert det(A) != 0, matrix_error
    
       
    # Computing c
    c = solve(A,b)
    
    # Fixing array elements of -0.0 to absolute value
    c[c == -0.] = abs(-0.) 
    
    return c





def cubic_interp(xi,yi):
    """
    Cubic interpolation.  Compute the coefficients of the polynomial
    interpolating the points (xi[i],yi[i]) for i = 0,1,2.
    Returns c, an array containing the coefficients of
      p(x) = c[0] + c[1]*x + c[2]*x**2 + c[3]*x**3.
    """

    # check inputs and print error message if not valid:

    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message

    error_message = "xi and yi should have length 4"
    assert len(xi)==4 and len(yi)==4, error_message
    

    # Set up linear system to interpolate through data points:
    A = np.vstack([np.ones(4), xi, xi**2, xi**3]).T
    b = yi
    
    # Check for singular matrix and print error message if matrix is singular:
    matrix_error = "Input xi ==> Singular matrix. No solutions!"
    assert det(A) != 0, matrix_error
    
       
    # Computing c
    c = solve(A,b)
    
    # Fixing array elements of -0.0 to absolute value
    c[c == -0.] = abs(-0.) 

    return c





def poly_interp(xi,yi):
    """
    (n-1)^th order polynomial interpolation.  Compute the coefficients of the polynomial
    interpolating the points (xi[i],yi[i]) for i = 0,1,2.
    Returns c, an array containing the coefficients of
      p(x) = c[0] + c[1]*x + c[2]*x**2 + c[3]*x**3 + ... + c[n-1]*x**(n-1)
      where n is the length of the coefficient array.
    """
    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message

    error_message = "xi and yi should have the same length"
    assert len(xi)==len(yi), error_message

    # Set up linear system to interpolate through data points:
    n = len(xi)
    matrix_A = np.vstack([xi**(i-1) for i in range(n, 0, -1)]).T
    A = np.fliplr(matrix_A)
    b = yi
    
    # Check for singular matrix and print error message if matrix is singular:
    matrix_error = "Input xi ==> Singular matrix. No solutions!"
    assert det(A) != 0, matrix_error
    
    # Computing c
    c = solve(A,b)
    
    # Fixing array elements of -0.0 to absolute value
    c[c == -0.] = abs(-0.) 

    return c
    
    



#########################################################################
#####     plot_*(xi,yi)     #############################################

def plot_quad(xi,yi):
    """
    Plotting the interpolating polynomial with data points input by user.
    """
    # Calling quad_interp to compute c
    c = quad_interp(xi,yi)
    print "Plotting data points and interpolating polynomial..."
    
    # Plot the resulting polynomial:
    x = np.linspace(xi.min() - 1, xi.max() + 1, 1000)   # points to evaluate polynomial
    y = c[0] + c[1]*x + c[2]*x**2
    
    # Using lambda so we can plot a generalized ylim for the plot window
    p = lambda x: c[0] + c[1]*x + c[2]*x**2
    
    plt.figure(1)       # open plot figure window
    plt.clf()           # clear figure
    plt.plot(x,y,'b-')  # connect points with a blue line
    
    # Add data points  (polynomial should go through these points!)
    #   the ylim references the min and max of the lambda function defined above
    plt.plot(xi,yi,'ro')   # plot as red circles
    plt.ylim(min(p(xi[:])) - 1, max(p(xi[:])) + 1)         # set limits in y for plot

    plt.title("Data points and interpolating quadratic polynomial")

    plt.savefig('quadratic.png')   # save figure as .png file
    plt.show()





def plot_cubic(xi,yi):
    """
    Plotting the interpolating polynomial with data points input by user.
    """
    # Calling quad_interp to compute c
    c = cubic_interp(xi,yi)
    print "Plotting data points and interpolating polynomial..."
    
    # Plot the resulting polynomial:
    x = np.linspace(xi.min() - 1, xi.max() + 1, 1000)   # points to evaluate polynomial
    y = c[0] + c[1]*x + c[2]*x**2 + c[3]*x**3
    
    plt.figure(1)       # open plot figure window
    plt.clf()           # clear figure
    plt.plot(x,y,'b-', scaley=False)  # connect points with a blue line
    
    # Add data points  (polynomial should go through these points!)
    plt.plot(xi,yi,'ro')   # plot as red circles
    # Turned off ylim to enable autoscaling
    #plt.ylim()


    plt.title("Data points and interpolating cubic polynomial")

    plt.savefig('cubic.png')   # save figure as .png file
    plt.show()
    
    
    
    
    
def plot_poly(xi,yi):
    """
    Plotting the interpolating polynomial with data points input by user.
    """
    # Calling quad_interp to compute c
    c = poly_interp(xi,yi)
    print "Plotting data points and interpolating polynomial..."
    
    # Plot the resulting polynomial implementing Horner's Rule:
    n = len(c)
    x = np.linspace(xi.min() - 1, xi.max() + 1, 1000)   # points to evaluate polynomial
    y = c[n-1]
    for j in range(n-1, 0, -1):         # Loop for using Horner's Rule
        y = y*x + c[j-1]
    
    plt.figure(1)       # open plot figure window
    plt.clf()           # clear figure
    plt.plot(x,y,'b-', scaley=False)  # connect points with a blue line
    
    # Add data points  (polynomial should go through these points!)
    plt.plot(xi,yi,'ro')   # plot as red circles
    # Turned off ylim to enable autoscaling
    #plt.ylim()


    plt.title("Data points and interpolating cubic polynomial")

    plt.savefig('poly.png')   # save figure as .png file
    plt.show()

    



##################################################################
#####     test_*()     ###########################################

def test_quad1():
    """
    Test code, no return value or exception if test runs properly.
    """
    xi = np.array([-1.,  0.,  2.])
    yi = np.array([ 1., -1.,  7.])
    c = quad_interp(xi,yi)
    c_true = np.array([-1.,  0.,  2.])
    print "c =      ", c
    print "c_true = ", c_true
    # test that all elements have small error:
    assert np.allclose(c, c_true), \
        "Incorrect result, c = %s, Expected: c = %s" % (c,c_true)
        
        
        

        
def test_quad2():
    """
    Test code, no return value or exception if test runs properly.
    """
    xi = np.array([3., -2., -5.])
    yi = np.array([-1., 6., 1.])
    c = quad_interp(xi,yi)
    c_true = np.array([5.5, -1.01666667, -0.38333333])
    print "c =      ", c
    print "c_true = ", c_true
    # test that all elements have small error:
    assert np.allclose(c, c_true), \
        "Incorrect result, c = %s, Expected: c = %s" % (c,c_true)     
   
   
   

   
def test_quad3():
    """
    Test code, no return value or exception if test runs properly.
    """
    xi = np.array([-4., 6., 0.])
    yi = np.array([0., 0., 0.])
    c = quad_interp(xi,yi)
    c_true = np.array([0., 0., 0])
    print "c =      ", c
    print "c_true = ", c_true
    # test that all elements have small error:
    assert np.allclose(c, c_true), \
        "Incorrect result, c = %s, Expected: c = %s" % (c,c_true)     
        
        


        
def test_cubic1():
    """
    Test code, no return value or exception if test runs properly.
    """
    xi = np.array([-3., -1., 0., 2.])
    yi = np.array([ 4., 3., -1., 0.])
    c = cubic_interp(xi,yi)
    c_true = np.array([-1., -3.56666667, 0.96666667, 0.53333333])
    print "c =      ", c
    print "c_true = ", c_true
    # test that all elements have small error:
    assert np.allclose(c, c_true), \
        "Incorrect result, c = %s, Expected: c = %s" % (c,c_true)
        




def test_cubic2():
    """
    Test code, no return value or exception if test runs properly.
    """
    xi = np.array([-1., 0., 2., -2.])
    yi = np.array([ 1., -1., 7., -9. ])
    c = cubic_interp(xi,yi)
    c_true = np.array([-1., -4., 0., 2.])
    print "c =      ", c
    print "c_true = ", c_true
    # test that all elements have small error:
    assert np.allclose(c, c_true), \
        "Incorrect result, c = %s, Expected: c = %s" % (c,c_true)
 
 
 
 
 
def test_cubic3():
    """
    Test code, no return value or exception if test runs properly.
    """
    xi = np.array([-1., 0., -3., 1.])
    yi = np.array([4., 0., -3., -9.])
    c = cubic_interp(xi,yi)
    c_true = np.array([0., -6.5, -2.5, 0.])
    print "c =      ", c
    print "c_true = ", c_true
    # test that all elements have small error:
    assert np.allclose(c, c_true), \
        "Incorrect result, c = %s, Expected: c = %s" % (c,c_true)     
        
        



def test_poly1():
    """
    Test code, no return value or exception if test runs properly.
    (n = 4)
    """
    xi = np.array([-1., 4., -2., 1.])
    yi = np.array([2., 0., -7., -3.])
    c = poly_interp(xi,yi)
    c_true = np.array([1.82222222, -3.25555556, -2.32222222, 0.75555556])
    print "c =      ", c
    print "c_true = ", c_true
    # test that all elements have small error:
    assert np.allclose(c, c_true), \
        "Incorrect result, c = %s, Expected: c = %s" % (c,c_true) 
        
        
        
        
        
def test_poly2():
    """
    Test code, no return value or exception if test runs properly.
    (n = 5)
    """
    xi = np.array([-1., 0., -3., 1.])
    yi = np.array([4., 0., -3., -9.])
    c = poly_interp(xi,yi)
    c_true = np.array([0., -6.5, -2.5, 0.])
    print "c =      ", c
    print "c_true = ", c_true
    # test that all elements have small error:
    assert np.allclose(c, c_true), \
        "Incorrect result, c = %s, Expected: c = %s" % (c,c_true)  
        
        
        
        
        
def test_poly3():
    """
    Test code, no return value or exception if test runs properly.
    (n = 6)
    """
    xi = np.array([1., -2., 0., -1., 5., -3.])
    yi = np.array([10., -5., -3., 4., 7., -9.])
    c = poly_interp(xi,yi)
    c_true = np.array([-3., -5.09821429, 9.83482143, 8.54017857, 0.16517857, -0.44196429])
    print "c =      ", c
    print "c_true = ", c_true
    # test that all elements have small error:
    assert np.allclose(c, c_true), \
        "Incorrect result, c = %s, Expected: c = %s" % (c,c_true)                





##################################################################
#####     "main"     #############################################

if __name__=="__main__":
    # "main program"
    # the code below is executed only if the module is executed at the command line,
    #    $ python demo2.py
    # or run from within Python, e.g. in IPython with
    #    In[ ]:  run demo2
    # not if the module is imported.
    print "Running test..."
    test_quad1()
    test_quad2()
    test_quad3()
    test_cubic1()
    test_cubic2()
    test_cubic3()
    test_poly1()
    test_poly2()
    test_poly3()
