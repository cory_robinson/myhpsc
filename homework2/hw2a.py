"""
$MYHPSC/homework2/hw2b.py

hw2b

Script for quadratic interpolation.
    This script takes data points, (x,y) coordinates, where x-coords are
    put in a numpy array, xi, and the y-coords are put in array yi.
    xi and yi both have length 3.  
    The xi array is then contained in the middle column of array A
    where the 1st column is all ones, and the third column is the 
    square of the middle column.  The system A|yi is then solved
    and the solutions are the array c whose elements are the coefficients
    quadratic polynomial interpolated through the data points given in 
    xi and yi.
    
    The interpolated polynomial is then plotted along with the data 
    points given by xi and yi.
    
Modified by: Cory R Robinson on 04/15/2013
"""



import numpy as np
import matplotlib.pyplot as plt
from numpy.linalg import solve


######################################################################
# Set up linear system to interpolate through data points:

# Data points:
xi = np.array([-1.,1.,2.])
yi = np.array([0.,4.,3.])

# It would be better to define A in terms of the xi points.
# Doing this is part of the homework assignment.
A = np.array([[1., -1. , 1.], [1., 1., 1.], [1., 2., 4.]])
b = yi

# Solve the system:
c = solve(A,b)

print "The polynomial coefficients are:"
print c


#####################################################################
# Plot the resulting polynomial:

x = np.linspace(-2,3,1001)   # points to evaluate polynomial
y = c[0] + c[1]*x + c[2]*x**2

plt.figure(1)       # open plot figure window
plt.clf()           # clear figure
plt.plot(x,y,'b-')  # connect points with a blue line

# Add data points  (polynomial should go through these points!)
plt.plot(xi,yi,'ro')   # plot as red circles
plt.ylim(-2,8)         # set limits in y for plot

plt.title("Data points and interpolating polynomial")

plt.savefig('hw2a.png')   # save figure as .png file
plt.show()
