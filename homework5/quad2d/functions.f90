! MYHPSC/homework5/quad2d/functions.f90
!
! Consists of functions used for numerical double integration by 
!	by the trapezoid rule. g is a funxtion of x and y, and is used
!	in the inner integral (dy integral). f is just a function of x
!	and is used in the outer integral.  f uses the trapezoid rule
!	to integrate g(x,y) with respect to y.  Then the quadrature.f90
!	module integrates f(x) with the trapezoid rule.
!
! Created by Cory Robinson on 5/21/2013
!
module functions

    use omp_lib
    implicit none
    integer :: fevals(0:7), gevals(0:7)
    real(kind=8) :: c, d
    save

contains

	real(kind=8) function g(x,y)
		implicit none
		real(kind=8), intent(in) :: x,y
		integer :: thread_num
		
		thread_num = 0 	! serial mode
		!$ thread_num = omp_get_thread_num()
		gevals(thread_num) = gevals(thread_num) + 1
		
		g = SIN(x + y)
		
	end function g
		

    real(kind=8) function f(x)
        implicit none
        real(kind=8), intent(in) :: x
        real(kind=8) :: h, trap_sum, yj
        integer :: j, thread_num
        integer, parameter :: ny = 1000
        
        thread_num = 0 	! serial mode
		!$ thread_num = omp_get_thread_num()
		fevals(thread_num) = fevals(thread_num) + 1
		
		
        
        ! implementing a trapezoid rule for integral of g(x,y)
        ! 	in order to return a value for f(x)
        h = (d-c)/(ny-1)
    	trap_sum = 0.5d0*(g(x,c) + g(x,d))  ! endpoint contributions
        
        do j=2, ny-1
        	yj = c + (j-1)*h
        	trap_sum = trap_sum + g(x,yj)
        	enddo
        
        f = h * trap_sum
        
    end function f

end module functions
