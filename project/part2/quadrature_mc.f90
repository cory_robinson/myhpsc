! $MYHPSC/project/part2/quadrature_mc.f90
!
! Created by Cory R Robinson on 6/4/2013
!
! Monte Carlo quadrature approximation
! 	This module is used to approximate n-dimensional integrals
!
module quadrature_mc

contains

real(kind=8) function quad_mc(g, a, b, ndim, npoints)
	implicit none
	real(kind=8), intent(in) :: a(ndim), b(ndim)
	integer, intent(in) :: ndim, npoints
	real(kind=8), external :: g
	integer :: n, i, j, k, h
	real(kind=4), allocatable :: r(:)
	real(kind=8), allocatable :: r_ab(:)
	real(kind=8) :: V, int_mc, s, x(ndim)

	k = ndim
	h = npoints
	n = ndim*npoints
	allocate(r(n))
	call random_number(r)
	allocate(r_ab(n))
	
	s = 0.d0
	do i=1, h
		! must index r so that it will have ndim elements for each i
		r_ab = a(k) + (b(k)-a(k))*r((i-1)*k+1:i*k)
		s = s + g(r_ab, ndim)
		enddo
	V = (b(k) - a(k))**k
	int_mc = (V / h)*s


end function quad_mc
	
end module quadrature_mc
