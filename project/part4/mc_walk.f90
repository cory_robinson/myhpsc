! $MYHPSC/project/part3/mc_walk.f90
!
! Created by Cory R Robinson on 6/9/2013
!
! Contains subroutines for the parallel MPI laplace_mc.f90 main program
!
module mc_walk

use mpi

! module variables
implicit none
integer :: nwalks, proc_num
save


contains


subroutine random_walk(i0, j0, max_steps, ub, iabort)

    !	This subroutine calls random_number and simulates a 2-D random walk
    ! on a lattice.  The subroutine returns values for
    !	ub - value of u where the walk hits a boundary on the lattice
    !	iabort - =0 if walk hit boundary
    !			 =1 if walk did not hit boundary within max_steps
    !
    use problem_description, only: utrue, uboundary,ax,ay,dx,dy,nx,ny,bx,by
    
    implicit none
    real(kind=8), intent(out) :: ub
    integer, intent(in) :: i0, j0, max_steps
    integer :: iabort, counts
    real(kind=4), allocatable :: r(:)
    real(kind=8) :: xb,yb
    integer :: i,j,istep
    
    logical :: debug1, debug2
    debug1 = .false.
    debug2 = .false.
	
    ! starting point
    i = i0
    j = j0

    ! generate as many random numbers as we could possibly need
    ! for this walk, since this is much faster than generating one at a time:
    allocate(r(max_steps))
	call random_number(r)
	
	if (debug1) then
		print *, "+++ generated r: ", r
		endif
	

	do istep = 1,max_steps
		! Take the next random step with equal probability in each
		! direction:
		if (r(istep) < 0.25d0) then
			i = i-1		! step left
			elseif (r(istep) < 0.5d0) then
				i = i+1		! step right
			elseif (r(istep) < 0.75d0) then
				j = j-1 	! step down
			else
				j = j+1		! step up
			endif
	
		! check if we hit the boundary			
		if (i*j*(nx+1-i)*(ny+1-j) == 0) then
			xb = ax + i*dx
			yb = ay + j*dy
			ub = uboundary(xb,yb)
			iabort = 0
			
			if (debug2) then
				counts = counts + 1
				print '("Hit boundary at (",es9.2,",",es9.2,") after ",i4, &
					" steps, ub = ",es14.6, i7)', xb, yb, istep+1, ub, counts
				endif
				exit
			
		elseif (istep==(max_steps+1)) then
			iabort = 1
			if (debug2) then
				print '("Did not hit boundary after ",i4," steps ")', max_steps
				endif
			exit
			endif
			
		enddo
	
	nwalks = nwalks + 1

end subroutine random_walk





subroutine many_walks(i0, j0, max_steps, n_mc, u_mc, n_success)

    !	This subroutine calculates values returned from the call to 
    ! the subroutine random_walks in this module.  The values returned are:
    !	u_mc - the sum of ub values calculated by calls to random_walks
    !	n_success - number of successfull random walks for each call to 
    !				random walks
    !	This subroutine is also parallelized using MPI, where the Master
    ! Process 0 farms out work for Process numbers 1,2,3,....
    !
    implicit none
    real(kind=8), intent(out) :: u_mc
    integer, intent(out) :: n_success
    integer, intent(in) :: i0, j0, max_steps, n_mc
    real(kind=8) :: ub_sum, ub
    integer :: i,j,k, iabort, z, zz, ierr, num_procs, numsent
	integer :: sender, nextwalk
	integer, dimension(MPI_STATUS_SIZE) :: status


call MPI_COMM_SIZE(MPI_COMM_WORLD, num_procs, ierr)
call MPI_COMM_RANK(MPI_COMM_WORLD, proc_num, ierr)    
call MPI_BCAST(n_mc, 1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)

    ub_sum = 0.d0	! to accumulate boundary values reached from all walks
    n_success = 0	! to keep track of how many walks reached boundary

	!------------------------------------------
    !	Code for Master Process 0
    !------------------------------------------

    if (proc_num == 0) then
		
		numsent = 0
		! send out the first batch of requests for random walks
		do i=1,min(num_procs-1,n_mc)
		call MPI_SEND(MPI_BOTTOM, 0, MPI_DOUBLE_PRECISION, &
                      i, 1, MPI_COMM_WORLD, ierr)
			numsent = numsent+1
			enddo

		! as results come back, send out more work...
        ! variable sender tells who sent back work and is ready for more
		do i = 1,n_mc
			call MPI_RECV(ub, 1, MPI_DOUBLE_PRECISION, MPI_ANY_SOURCE, &
						MPI_ANY_TAG, MPI_COMM_WORLD, status, ierr)
			sender = status(MPI_SOURCE)
			zz = status(MPI_TAG)

			if (zz == 0) then
    				ub_sum = ub_sum + ub
    				n_success = n_success + 1
					u_mc = ub_sum / n_success
    			elseif (zz==1) then 
					! unsuccessful walk, no values to accumulate
				endif


			if (numsent < n_mc) then
        			! still more work to do, next request for walks will be sent
				! out and this index is also used as the tag
				call MPI_SEND(MPI_BOTTOM, 0, MPI_DOUBLE_PRECISION, &
                      		sender, 1, MPI_COMM_WORLD, ierr)
				numsent = numsent + 1
			else
        		! send an empty message with tag=0 to indicate this worker
        		! is done.
        		call MPI_SEND(MPI_BOTTOM, 0, MPI_DOUBLE_PRECISION,&
                            sender, 0, MPI_COMM_WORLD, ierr)
            		endif
			enddo
		
		endif 


	!------------------------------------------
	!	Code for Worker Processors 1, 2, ...
	!------------------------------------------
	
	if (proc_num /= 0) then

		if (proc_num > n_mc) go to 99 ! no work expected
		
		do while (.true.)
			! repeat until message with tag = 0 is received
			call MPI_RECV(MPI_BOTTOM, 0, MPI_DOUBLE_PRECISION, 0, &
						MPI_ANY_TAG, MPI_COMM_WORLD, status, ierr)

			z = status(MPI_TAG)
					
			if (z == 0) go to 99	! No more work

			i = i0
    		j = j0
			call random_walk(i,j,max_steps,ub,iabort)
				
			call MPI_SEND(ub, 1, MPI_DOUBLE_PRECISION, 0, iabort, &
					   		MPI_COMM_WORLD, ierr)
				
			enddo
		endif
    				
	!+++++++++++++++++++++++++++++++++++++++++++++
	! END OF MASTER/WORKER FARMING
	!+++++++++++++++++++++++++++++++++++++++++++++
    
99 continue		! May jump here if done early

end subroutine many_walks

end module mc_walk
