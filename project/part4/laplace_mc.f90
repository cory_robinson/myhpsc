! $MYHPSC/project/part3/laplace_mc.f90
!
! Created by Cory R Robinson on 6/7/2013
!
! This is an MPI paralllel version of a code that approximated the solution
!	to the Laplace equation at a particular point using Monte Carlo Methods
! 
program laplace_mc

use mpi



    use problem_description, only: utrue, uboundary, nx,ny,ax,ay,dx,dy,bx,by
	use mc_walk, only: random_walk, many_walks, nwalks, proc_num
	use random_util, only: init_random_seed

    implicit none
    integer :: n_success, ierr, num_procs, nwalks_proc
    integer :: i0,j0, max_steps, n_mc, i, j, n_total, nwalks_total
    integer :: seed1, proc_nums
    real(kind=8) :: x0,y0, u_mc_total, u_mc, u_true, error
    real(kind=8) :: u_sum_old, u_sum_new

call MPI_INIT(ierr)
call MPI_COMM_SIZE(MPI_COMM_WORLD, num_procs, ierr)
call MPI_COMM_RANK(MPI_COMM_WORLD, proc_num, ierr)

    
    ! initialize nwalks; end result is how many times random walks is called
    nwalks = 0
    open(unit=25, file='mc_laplace_error.txt', status='unknown')
    
    ! Try it out from a specific (x0,y0)
    x0 = 0.9d0
    y0 = 0.6d0
    
    i0 = nint((x0-ax)/dx)	! nint --> rounding to nearest integer
    j0 = nint((y0-ay)/dy)

    u_true = utrue(x0,y0)

	if (num_procs == 1) then
		print *, "*** Error, this program requires more than 1 processor! "
		call MPI_FINALIZE(ierr)
		stop
		endif 

	if (proc_num == 0) then
		print '("Using ",i3," processes")', num_procs
		print *, ! blank line
    		print '("True solution of PDE: u(",e7.1,",",e7.1,") =",e15.5)', &
				x0,y0,u_true
    		print *,"Note: with solution used in demo this is also the solution "
    		print *,"	the finite-difference equations on the same grid. "
		print *, ! blank line
		endif

call MPI_BARRIER(MPI_COMM_WORLD,ierr) ! wait for process 0 to print	
    
    ! Set seed for random number generator
    seed1 = 12345   ! or set to 0 for random seed
	seed1 = seed1 + 97*proc_num		! unique for each process
    call init_random_seed(seed1)
    
    ! maximum number of steps in each bfore giving up
    max_steps = 	100*max(nx,ny)
    
    ! initial number of Monte-Carlo walks to take:
    n_mc = 10
	nwalks=0	! keep up w/ total number of walks
    
    call many_walks(i0, j0, max_steps, n_mc, u_mc, n_success)
	if (proc_num==0) then
    	error = abs((u_mc - u_true) / u_true)
    
    	! start accumulating totals:
    	u_mc_total = u_mc
   	 	n_total = n_success

    	print '(i10,e23.15,e15.6)', n_total, u_mc_total, error
    	write(25, '(i10,e23.15,e15.6)') n_total, u_mc_total, error
		endif

    do i = 1,12
		u_sum_old = u_mc_total * n_total
		call many_walks(i0, j0, max_steps, n_mc, u_mc, n_success)		
		u_sum_new = u_mc * n_success
		n_total = n_total + n_success
		u_mc_total = (u_sum_old + u_sum_new) / n_total
		error = abs((u_mc_total - u_true) / u_true)
		if (n_total > 0) then
			write(25, '(i10,e23.15,e15.6)') n_total, u_mc_total, error
			print '(i10,e23.15,e15.6,e24.16,e15.6)', n_total, u_mc_total, &
					error
			endif
		n_mc = 2*n_mc	! double number of trials for next iteration
		enddo 

!-------------------------------------------------------
! Final print statements
!-------------------------------------------------------
call MPI_BARRIER(MPI_COMM_WORLD,ierr) ! wait for approximations to print
	
    call MPI_REDUCE(nwalks, nwalks_total,1, &
       				MPI_INTEGER, MPI_SUM, 0, &
      				MPI_COMM_WORLD, ierr)
      				
	if (proc_num==0) then
    		print '("Final approximation to u(x0,y0): ",es23.14)', u_mc_total
		print *, "Total walks performed by all processes: ", nwalks_total
		endif
	
call MPI_BARRIER(MPI_COMM_WORLD,ierr) ! wait for process 0 to print

	! print the number of function evaluations by each thread:
    print '("Walks performed by process ",i2,": ",i13)',  proc_num, nwalks



call MPI_FINALIZE(ierr)

end program laplace_mc
