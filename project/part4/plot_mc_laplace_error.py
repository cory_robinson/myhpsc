# $MYHPSC/project/part4/plot_mc/laplace_error.py
#
# Created by Cory R Robinson on 6/10/2013
#
# Reads in data into a .txt file and produces a plot of the data
#
from pylab import *

# read in three columns from file and unpack into 3 arrays:
n,int_approx,error = loadtxt('mc_laplace_error.txt',unpack=True)

figure(1)
clf()
loglog(n,error,'-o',label='Monte-Carlo')
loglog([1,1e7],[1,sqrt(1e-7)],'k',label='1 / sqrt(N)')
legend()
xlabel('number of random walks taken')
ylabel('abs(error)')
title('Log-log plot of relative error in laplace MC solutions')
savefig('mc_laplace_error.png')
