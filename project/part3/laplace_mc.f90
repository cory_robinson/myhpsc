! $MYHPSC/project/part3/laplace_mc.f90
!
! Created by Cory R Robinson on 6/7/2013
!
! Main program for solving the Laplace eqn at a point (x0,y0) 
!	implementing a Monte Carlo Method
!
!	A Makefile is used with this program:
!		make test - prints output in the shell and to the file
!					mc_laplace_error.txt
!		make plot - does same as make test and in adition creates 
!					the image file mc_laplace_error.png
! 
program laplace_mc

    use problem_description, only: utrue, uboundary, nx,ny,ax,ay,dx,dy,bx,by
	use mc_walk, only: random_walk, many_walks, nwalks
	use random_util, only: init_random_seed

    implicit none
    integer :: n_success
    integer :: i0,j0, max_steps, n_mc, i, j, n_total
    integer :: seed1
    real(kind=8) :: x0,y0, u_mc_total, u_mc, u_true, error
    real(kind=8) :: u_sum_old, u_sum_new
    
    
    ! initialize nwalks; end result is how many times random walks is called
    nwalks = 0
    open(unit=25, file='mc_laplace_error.txt', status='unknown')
    
    ! Try it out from a specific (x0,y0)
    x0 = 0.9d0
    y0 = 0.6d0
    
    i0 = nint((x0-ax)/dx)	! nint --> rounding to nearest integer
    j0 = nint((y0-ay)/dy)

    u_true = utrue(x0,y0)

    print '("True solution of PDE: u(",e7.1,",",e7.1,") =",e15.5)',x0,y0,u_true
    print *, "Note: with solution used in demo this is also the solution to the"
    print *, "	finite-difference equations on the same grid. "
    
    ! Set seed for random number generator
    seed1 = 12345   ! or set to 0 for random seed
    call init_random_seed(seed1)
    
    ! maximum number of steps in each bfore giving up
    max_steps = 100*max(nx,ny)
    
    ! initial number of Monte-Carlo walks to take:
    n_mc = 10
    nwalks = 0
    
    call many_walks(18, 4, 1900, 10, u_mc, n_success)
    error = abs((u_mc - u_true) / u_true)
    
    ! start accumulating totals:
    u_mc_total = u_mc
    n_total = n_success
    
    print '(i10,e23.15,e15.6)', n_mc, u_mc_total, error
    write(25, '(i10,e23.15,e15.6)') n_total, u_mc_total, error

    do i = 1,12
		u_sum_old = u_mc_total * n_total
		call many_walks(i0, j0, max_steps, n_mc, u_mc, n_success)		
		u_sum_new = u_mc * n_success
		n_total = n_total + n_success
		u_mc_total = (u_sum_old + u_sum_new) / n_total
		error = abs((u_mc_total - u_true) / u_true)
		write(25, '(i10,e23.15,e15.6)') n_total, u_mc_total, error
		print '(i10,e23.15,e15.6)', n_total, u_mc_total, error
		n_mc = 2*n_mc	! double number of trials for next iteration
		enddo 

    print '("Final approximation to u(x0,y0): ",e23.14)', u_mc_total
	print *, "Total number of random walks: ", nwalks

end program laplace_mc
