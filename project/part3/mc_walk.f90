
module mc_walk

! module variables
implicit none
integer :: nwalks
save


contains


subroutine random_walk(i0, j0, max_steps, ub, iabort)
    !	This subroutine calls random_number and simulates a 2-D random walk
    ! on a lattice.  The subroutine returns values for
    !	ub - value of u where the walk hits a boundary on the lattice
    !	iabort - =0 if walk hit boundary
    !			 =1 if walk did not hit boundary within max_steps
    !
    use problem_description, only: utrue, uboundary,ax,ay,dx,dy,nx,ny,bx,by
    
    implicit none
    real(kind=8), intent(out) :: ub
    integer, intent(in) :: i0, j0, max_steps
    integer :: iabort, counts
    real(kind=4), allocatable :: r(:)
    real(kind=8) :: xb,yb
    integer :: i,j,istep
    
    logical :: debug1, debug2
    debug1 = .false.
    debug2 = .false.
	
    ! starting point
    i = i0
    j = j0
! i and j are correct
! max_steps is correct
! ALL INPUT VARIABLES ARE CORRECT
    ! generate as many random numbers as we could possibly need
    ! for this walk, since this is much faster than generating one at a time:
    allocate(r(max_steps))
	call random_number(r)
	
	if (debug1) then
		print *, "+++ generated r: ", r
		endif
	
counts = 0
	do istep = 1,max_steps
		! Take the next random step with equal probability in each
		! direction:
		if (r(istep) < 0.25d0) then
			i = i-1		! step left
			elseif (r(istep) < 0.5d0) then
				i = i+1		! step right
			elseif (r(istep) < 0.75d0) then
				j = j-1 	! step down
			else
				j = j+1		! step up
			endif
	
		! check if we hit the boundary			
		if (i*j*(nx+1-i)*(ny+1-j) == 0) then
			xb = ax + i*dx
			yb = ay + j*dy
			ub = uboundary(xb,yb)
			
				
			iabort = 0
			
			if (debug2) then
				counts = counts + 1
				print '("Hit boundary at (",e9.2,",",e9.2,") after ",i4, &
					" steps, ub = ",e14.6, i7)', xb, yb, istep+1, ub, counts
				endif
				exit
			
		elseif (istep==(max_steps+1)) then
			iabort = 1
			if (debug2) then
				print '("Did not hit boundary after ",i4," steps ")', max_steps
				endif
			exit
			endif
			!if (iabort==1) cycle
			
		enddo
	nwalks = nwalks + 1

end subroutine random_walk





subroutine many_walks(i0, j0, max_steps, n_mc, u_mc, n_success)
	!	This subroutine calculates values returned from the call to 
    ! the subroutine random_walks in this module.  The values returned are:
    !	u_mc - the sum of ub values calculated by calls to random_walks
    !	n_success - number of successfull random walks for each call to 
    !				random walks
    !
    implicit none
    real(kind=8), intent(out) :: u_mc
    integer, intent(out) :: n_success
    integer, intent(in) :: i0, j0, max_steps, n_mc
    real(kind=8) :: ub_sum, ub
    integer :: i,j,k, iabort

    ub_sum = 0.d0	! to accumulate boundary values reached from all walks
    n_success = 0	! to keep track of how many walks reached boundary
    
    do k = 1,n_mc
    	i = i0
    	j = j0

    	call random_walk(i,j,max_steps,ub,iabort)
    	
    	if (iabort == 0) then
    		ub_sum = ub_sum + ub
    		n_success = n_success + 1
    		endif
    		
    	
    	!if (iabort == 1) then
    		! use this result unless walk didn't reach boundary
    	!	endif
    	enddo

    u_mc = ub_sum / n_success
    !print *, "u_mc =   ", u_mc
    

end subroutine many_walks

end module mc_walk
