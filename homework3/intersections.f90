! $UWHPSC/codes/fortran/newton/intersections.f90

program intersections

    use newton, only: solve
    use functions, only: g1, g2, g1prime, g2prime, g_intersect, gprime_intersect

    implicit none
    real(kind=8) :: x, x0, gx
    real(kind=8) :: x0vals(4)
    integer :: iters, itest
	logical :: debug         ! set to .true. or .false.
	
	

    print *, "Computing intersections of functions g1(x) and g2(x)..."
    ! debug turned off. For output on each iteration, set debug=.true.
    debug = .false. 

    ! values to test as x0:
    x0vals = (/-2.2d0, -1.6d0, -0.8d0, 1.4d0 /)
    

	
    do itest=1,4
        x0 = x0vals(itest)
		print *, ' '  ! blank line
        call solve(g_intersect, gprime_intersect, x0, x, iters, debug)
        
        print 11, x0
11		format('With initial guess x0 = ', e22.15, ',') 

        print 12, x, iters
12      format('	solve returns x = ', e22.15, ' after', i3, ' iterations')


        if (abs(g1(x) - g2(x)) > 1d-14) then
            print 13, x
13          format('*** Unexpected result: x = ', e22.15)
            endif
        enddo

end program intersections
