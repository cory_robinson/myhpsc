"""
$MYHPSC/homework3/intersections.py

intersections

Solves for the intersection of two functions g1 and g2, by using the module
	newton.py.     
    
Created by: Cory R Robinson on 04/25/2013

========================================================
EXAMPLE:



========================================================


"""


import numpy as np
import matplotlib.pyplot as plt
import newton


########################################################################
#####     intersect1(x)     #################################################
### NOT USED RUNNING AS A SCRIPT###
## Example from notes on HW#3 page. ##
def intersect1(x):
	"""
	Returns values of f and f' from example on homework #3 page.
	"""
	
	g1 = np.sin(x)
	g2 = 1 - x**2
	
	g1p = np.cos(x)
	g2p = -2*x
	
	f = g1 - g2
	fp = g1p - g2p
	
	return f,fp 






#########################################################################
#####     gfuncs(x)     ####################################################

def gfuncs(x):
	"""
	Returns values of functions g1, g2, and their first derivatives.
	"""
	# Defining g1(x) and g2(x)
	g1 = x*np.cos(np.pi*x)
	g2 = 1 - 0.6*x**2
	
	# Defining the derivatives of g*(x)
	g1p = np.cos(np.pi*x) - np.pi*x*np.sin(np.pi*x)
	g2p = -2*0.6*x
	
	return g1, g2, g1p, g2p


########################################################################
#####     intersect2(x)     ############################################
## RENAME AT LATER TIME ##
def intersect_funcs(x):
	"""
	Returns values of the intersections of g1, g2, and the intersections
		of their derivatives.
	"""
	# defines the intersections of g1, g2 AND g1p, g2p
	f = gfuncs(x)[0] - gfuncs(x)[1]
	fp = gfuncs(x)[2] - gfuncs(x)[3]
	
	return f,fp 
	
	
	
	

########################################################################
#####     calling newton.solve() for intersect2     ####################

def intersect_solve():
	"""
	Returns calls the newton module to return the intersection points
		of the functions g1 and g2.
	"""
	# solves intersection of g functions
	for x0 in [-2.2, -1.6, -.8, 1.4]:
		print " "  # blank line
		x,iters = newton.solve(intersect_funcs, x0, debug=False)
		print "With initial guess x0 = %22.15e, " % x0
		print "	solve returns x = %22.15e after %i iterations " % (x,iters)
		
		# plot intersection points as black dots
		plot_points = plt.plot(x, gfuncs(x)[0],'ko')
	
	return x, plot_points



########################################################################
#####     plotting for intersections.py     ############################


def plot_intersection():
	"""
	Plots the functions g1 and g2, and the points of their intersections
		obtained from intersect_solve()
	"""
	x = np.linspace(-4, 4, 1000)   # points to evaluate polynomial

	plt.figure(1)       # open plot figure window
	plt.clf()           # clear figure
	plt.plot(x,gfuncs(x)[0])
	plt.plot(x,gfuncs(x)[1],'r')
	
	# Adding intersection points as black dots
	#   This also call intersect_solve() to print output !!! FIX BUG
	intersect_solve()[1]
	
	plt.title("Intersections of functions g1(x) and g2(x)")
	plt.savefig('intersections.png')    # save figure as .png file
	plt.show()	




########################################################################
#####     test1     ####################################################

def test1(debug_solve=False):
    """
    Test Newton iteration for the square root with different initial
    conditions.
    """
    from numpy import sqrt
    for x0 in [-5., 5.]:
        print " "  # blank line
        x,iters = newton.solve(fvals, x0, debug=debug_solve)
        print "solve returns x = %22.15e after %i iterations " % (x,iters)
        fx,fpx = fvals(x)
        print "the value of f(x) is %22.15e" % fx
        #assert (abs(x-1.) < 1e-14) or (abs(x-6) < 6e-14), "*** Unexpected result: x = %22.15e"  % x	



##################################################################
#####     "main"     #############################################

if __name__=="__main__":
    # "main program"
    # the code below is executed only if the module is executed at the command line,
    #    $ python intersections.py
    # or run from within Python, e.g. in IPython with
    #    In[ ]:  run intersections.py
    # not if the module is imported.
    print "Running script..."
    plot_intersection()
