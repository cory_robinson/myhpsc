! $MYHPSC/homework3/functions.f90
!
! Modified by Cory R Robinson on 5/1/2013

module functions


contains


real(kind=8) function f_sqrt(x)
    implicit none
    real(kind=8), intent(in) :: x

    f_sqrt = x**2 - 4.d0

end function f_sqrt


real(kind=8) function fprime_sqrt(x)
    implicit none
    real(kind=8), intent(in) :: x
    
    fprime_sqrt = 2.d0 * x

end function fprime_sqrt


real(kind=8) function g1(x)
	implicit none
	real(kind=8), intent(in) :: x
	real(kind=8) :: pi
	pi = ACOS(-1.d0)
	
	g1 = x*COS(pi*x)
	
end function g1


real(kind=8) function g2(x)
	implicit none
	real(kind=8), intent(in) :: x
	
	g2 = 1.d0 - 0.6d0*x**2
	
end function g2


real(kind=8) function g1prime(x)
	implicit none
	real(kind=8), intent(in) :: x
	real(kind=8) :: pi
	pi = ACOS(-1.d0)
	
	g1prime = COS(pi*x) - pi*x*SIN(pi*x)
	
end function g1prime


real(kind=8) function g2prime(x)
	implicit none
	real(kind=8), intent(in) :: x
	
	g2prime = -2*0.6*x
	
end function g2prime


real(kind=8) function g_intersect(x)
	implicit none
	real(kind=8), intent(in) :: x
	real(kind=8) :: pi
	pi = ACOS(-1.d0)
	
	g_intersect = g1(x) - g2(x)
	
end function g_intersect


real(kind=8) function gprime_intersect(x)
	implicit none
	real(kind=8), intent(in) :: x
	real(kind=8) :: pi
	pi = ACOS(-1.d0)
	
	gprime_intersect = g1prime(x) - g2prime(x)
	
end function gprime_intersect



end module functions
