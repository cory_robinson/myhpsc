! $MYHPSC/homework3/am583/functions.f90

! This file contains mathematical functions defined for use
!   in solving for zeros of functions and also intersection 
!   points of some functions.
!
! Modified by Cory R. Robinson 4/28/2013

module functions


! module variables:
real(kind=8) :: epsilon
    save


contains


real(kind=8) function f_sqrt(x)
    implicit none
    real(kind=8), intent(in) :: x

    f_sqrt = x**2 - 4.d0

end function f_sqrt


real(kind=8) function fprime_sqrt(x)
    implicit none
    real(kind=8), intent(in) :: x
    
    fprime_sqrt = 2.d0 * x

end function fprime_sqrt


real(kind=8) function g1(x)
	implicit none
	real(kind=8), intent(in) :: x
	real(kind=8) :: pi
	pi = ACOS(-1.d0)
	
	g1 = x*COS(pi*x)
	
end function g1


real(kind=8) function g2(x)
	implicit none
	real(kind=8), intent(in) :: x
	
	g2 = 1.d0 - 0.6d0*x**2
	
end function g2


real(kind=8) function g1prime(x)
	implicit none
	real(kind=8), intent(in) :: x
	real(kind=8) :: pi
	pi = ACOS(-1.d0)
	
	g1prime = COS(pi*x) - pi*x*SIN(pi*x)
	
end function g1prime


real(kind=8) function g2prime(x)
	implicit none
	real(kind=8), intent(in) :: x
	
	g2prime = -2*0.6*x
	
end function g2prime


real(kind=8) function g_intersect(x)
	implicit none
	real(kind=8), intent(in) :: x
	real(kind=8) :: pi
	pi = ACOS(-1.d0)
	
	g_intersect = g1(x) - g2(x)
	
end function g_intersect


real(kind=8) function gprime_intersect(x)
	implicit none
	real(kind=8), intent(in) :: x
	real(kind=8) :: pi
	pi = ACOS(-1.d0)
	
	gprime_intersect = g1prime(x) - g2prime(x)
	
end function gprime_intersect


real(kind=8) function f_quartic(x)
	implicit none
	real(kind=8), intent(in) :: x
	
	f_quartic = (x - 1)**4 - epsilon
	
end function f_quartic


real(kind=8) function fprime_quartic(x)
	implicit none
	real(kind=8), intent(in) :: x
	
	fprime_quartic = 4*(x-1)**3
	
end function fprime_quartic



end module functions
