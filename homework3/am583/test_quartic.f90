! $MYHPSC/homework3/am583/test_quartic.f90
!
! This program is used to solve for the zeros of a quartic polynomial
!   f(x) = (x-1)**4 - epsilon   and tests the convergence for various
!   values of epsilon.
!
! Created by Cory R. Robinson on 4/28/2013

program test_quartic

    use newton, only: tol, solve
    use functions, only: epsilon, f_quartic, fprime_quartic

    implicit none
    real(kind=8) :: x, x0, fx, f_actual, xstar, error
    real(kind=8) :: epsilonvals(3)
    real(kind=8) :: tolvals(3)
    integer :: iters, i, j
	logical :: debug         ! set to .true. or .false.

    print *, "Test routine for computing zeros of f_quartic(x)..."
    ! debug turned off. To see output of all iterations set debug = .true.
    debug = .false.

    ! values to test as epsilon:
    epsilonvals = (/0.100D-03, 0.100D-07, 0.100D-11 /)
    
    ! values to test as tol:
    tolvals = (/0.100D-04, 0.100D-09, 0.100D-13 /)
    
    ! value of initial guess
    x0 = 4.d0
    print *, ''   ! blank line
    print 11, x0
11	format('Starting with initial guess ', d22.15)
	print *, ''   ! blank line
	
	! prints table headings:
	print *, '    epsilon        tol    iters          x                 f(x)        x-xstar'
    
    do i=1,3
    	do j=1,3
    		
    		! iterating epsilon and to thru their arrays *vals defined above
        	epsilon = epsilonvals(i)
        	tol = tolvals(j)
        	
			! calling solve from newton module to find zeros of f_quartic
        	call solve(f_quartic, fprime_quartic, x0, x, iters, debug)
        	
        	! produces the f(x) value in the table that's printed out
        	fx = f_quartic(x)
        	
        	! Defining the error x-xstar
        	xstar = 1 + epsilon**0.25d0
        	error = x - xstar

			print 12, epsilon, tol, iters, x, fx, error
12			format(2d13.3, i4, d24.15, 2d13.3)
			
        enddo
        print *, ''   ! blank line
	enddo

end program test_quartic
