"""
$MYHPSC/homework3/newton.py

newton

Module that implements newton's method for finding the zeros of 
functions.  Running this as a script will output the roots of 
f(x) = x**2 - 4.      
    
Created by: Cory R Robinson on 04/23/2013

"""


import numpy as np


#########################################################################
#####     fvals_sqrt(x)     #############################################

def fvals_sqrt(x):
	"""
	Returns values of f(x) = x**2 - 4, and f'(x) = 2*x, for inputs of x.
	"""
	f = x**2 - 4.
	fp = 2.*x
	return f,fp
	




#########################################################################
#####     solve(fvals, x0, debug=False)     #############################

def solve(fvals, x0, debug=False):
	"""
	Solves, using newtons method, the zeros of a function input as an array
		in the argument fvals, with initial guess x0. Setting debug=True(False) 
		will show (not show) output after each iteration of newtons method.
	"""
	
	# Initial guess
	x = x0
	
	# Tolerance
	tol = 1.e-14
	
	if debug:
		print "Initial guess: x = %22.15e " % x
	
	# Newton iteration to find zeros of f(x)
	maxiter = 20
	for k in range(1,maxiter):
	
		# Evaluate a function and its derivative
		fx = fvals(x)[0]
		fxprime = fvals(x)[1]
		
		# break out of loop if abs(fx) is less than the tolerance
		if abs(fx) < tol:
			break 
				
		# Newton increment x:
		deltax = fx/fxprime
		
		# Update x:
		x = x - deltax
		
		if debug:
			print "After %i iterations, x = %22.15e " % (k,x)
	
	if k > maxiter:
		fx = fvals(x)[0]
		if abs(fx) > tol:
			print "*** Warning:  has not yet converged"
	
	# Number of iterations	
	iters = k-1
	
	return x, iters
		
		

########################################################################
#####     test1     ####################################################

def test1(debug_solve=False):
    """
    Test Newton iteration for the square root with different initial
    conditions.
    """
    from numpy import sqrt
    for x0 in [1., 2., 100.]:
        print " "  # blank line
        x,iters = solve(fvals_sqrt, x0, debug=debug_solve)
        print "solve returns x = %22.15e after %i iterations " % (x,iters)
        fx,fpx = fvals_sqrt(x)
        print "the value of f(x) is %22.15e" % fx
        assert abs(x-2.) < 1e-14, "*** Unexpected result: x = %22.15e"  % x	
		
