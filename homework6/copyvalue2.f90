! $MYHPSC/homework6/copyvalue2.f90
!
! Provided by RJ Leveque on 5/25/2013
! Modified by Cory R Robinson on 5/29/2013
!
! Set value i in Process (num_procs - 1) and passes the 
! value i-1 backwards through a chain of processes to Process 0.
!

program copyvalue

    use mpi

    implicit none

    integer :: i, proc_num, num_procs,ierr
    integer, dimension(MPI_STATUS_SIZE) :: status

    call MPI_INIT(ierr)
    call MPI_COMM_SIZE(MPI_COMM_WORLD, num_procs, ierr)
    call MPI_COMM_RANK(MPI_COMM_WORLD, proc_num, ierr)

    if (num_procs==1) then
        print *, "Only one process, cannot do anything"
        call MPI_FINALIZE(ierr)
        stop
        endif


    if (proc_num == num_procs - 1) then
        i = 55
        print '("Process ",i3," setting      i = ",i3)', proc_num, i

        call MPI_SEND(i, 1, MPI_INTEGER, num_procs - 2, 21, &
                      MPI_COMM_WORLD, ierr)
                      

      else if (proc_num < (num_procs -1) .AND. (proc_num > 0)) then

        call MPI_RECV(i, 1, MPI_INTEGER, proc_num+1, 21, &
                      MPI_COMM_WORLD, status, ierr)

        print '("Process ",i3," receives     i = ",i3)', proc_num, i
        print '("Process ",i3," sends        i = ",i3)', proc_num, i-1

        call MPI_SEND(i-1, 1, MPI_INTEGER, proc_num-1, 21, &
                      MPI_COMM_WORLD, ierr)
                      
               
      else if (proc_num == 0) then

        call MPI_RECV(i, 1, MPI_INTEGER, proc_num+1, 21, &
                      MPI_COMM_WORLD, status, ierr)

        print '("Process ",i3," ends up with i = ",i3)', proc_num, i

      endif

    call MPI_FINALIZE(ierr)

end program copyvalue
